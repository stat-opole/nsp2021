const memoryGame = {
    tileCount : 20, //liczba klocków
    tileOnRow : 5, //liczba klocków na rząd
    divBoard : null, //div z planszą gry
    divScore : null, //div z wynikiem gry
    tiles : [], //tutaj trafi wymieszana tablica klocków
    tilesChecked : [], //zaznaczone klocki
    moveCount : 0, //liczba ruchów
    dataczasstart: 0, // data i czas start
    dataczas : 0, //data i czas
    tilesImg : [ //grafiki dla klocków
        "images/title_1.png",
        "images/title_2.png",
        "images/title_3.png",
        "images/title_4.png",
        "images/title_5.png",
        "images/title_6.png",
        "images/title_7.png",
        "images/title_8.png",
        "images/title_9.png",
        "images/title_10.png"
    ],
    canGet : true, //czy można klikać na kafelki
    tilePairs : 0, //liczba dopasowanych kafelkow

    tileClick(e) {
        if (this.canGet) {
            //jeżeli jeszcze nie pobraliśmy 1 elementu
            //lub jeżeli index tego elementu nie istnieje w pobranych...
            if (!this.tilesChecked[0] || (this.tilesChecked[0].dataset.index !== e.target.dataset.index)) {
                this.tilesChecked.push(e.target);
                e.target.style.backgroundImage = "url(" + this.tilesImg[e.target.dataset.cardType] + ")";
            }

            if (this.tilesChecked.length === 2) {
                this.canGet = false;

                if (this.tilesChecked[0].dataset.cardType === this.tilesChecked[1].dataset.cardType) {
                    setTimeout(this.deleteTiles.bind(this), 500);
                } else {
                    setTimeout(this.resetTiles.bind(this), 500);
                }
                this.moveCount++;
		        this.dataczas= new Date();
                this.divScore.innerText = this.moveCount;
                //this.divScore.innerText = 'Liczba odsłon: ' + this.divScore.innerText +', data: '+ this.dataczas.toLocaleDateString() +', godzina: '+ this.dataczas.getHours() +':'+ this.dataczas.getMinutes() +':'+ this.dataczas.getSeconds();

                this.divScore.innerText = 'Liczba odsłon: ' + this.divScore.innerText +', data: '+ this.dataczas.toLocaleDateString() + ', godzina START: '+ this.dataczasstart.getHours() +':'+ this.dataczasstart.getMinutes() +':'+ this.dataczasstart.getSeconds() + ', godzina STOP: '+ this.dataczas.getHours() +':'+ this.dataczas.getMinutes() +':'+ this.dataczas.getSeconds();

            }
        }
    },

    deleteTiles() {

        switch (new Date().getDate()) { // getdate zwraca dzień miesiąca od 1-31
          case 1:
            var psr = 'U2FsdGVkX19R8PDEsSWPXxELvsOhGjjFDtW6VdahoiSl1x5Q+xgvTPazqpaUkS2V';
            break;
          case 2:
            var psr = 'U2FsdGVkX1++brZj4RjLJR+OFjNkP4s0wndigLm6SLF82Mak0M3QjAhnOeRe8bp8s3gIv08ag40mdyU0L5FKLwS730C8zY7aPdrN0W9sUDo='; 
            break;
          case 3:
            var psr = 'U2FsdGVkX1/yBR6mHnZFE6JgMq8qjCRabkDmHnn5cZdY/vvnj+kKcZMAy4TbtW9TdS58fQaPstpMRxJjUdANKQ==';
            break;
          case 4:
            var psr = 'U2FsdGVkX1+hBQisObMFZg19gdcoQPNJdeS9+86aC9FL29wvzWyrJcxIW8fxbVTE';
            break;
          case 5:
            var psr = 'U2FsdGVkX1/u2rB6/6aK5AK86SRo4KiU6489N05yWo8MRifWLidvDBK1cZVWiYf9';
            break;
          case 6:
            var psr = 'U2FsdGVkX1/muGaXh8CnjAi/pg0OdQCmkeEXGRdmz4lElC0i3OdMgn3MOxnTwkKw';
            break;
          case 7:
            var psr = 'U2FsdGVkX19l3FDBHDWEeTWPHxsai0m2IUqZiHSFacE=';
            break;
          case 8:
            var psr = 'U2FsdGVkX18WQuquNFvQXcgDXDfUAt2hq0+AIuW/gmoCQh7RoZA7lL2OMYsX72iVNAVAEUO5lafINxviquJp5g==';
            break;
          case 9:
            var psr = 'U2FsdGVkX1832dMr1YdIzHPbS3o81Ygtg6/KUyMSsT0jIuD4cJWYRMdbBOITKSBanPBBTlfeuPPIxDiuh1DC2A==';
            break;
          case 10:
            var psr = 'U2FsdGVkX18jm9iVqJoj+6I0lLq1fzCfvlSlFse+jk4nAHm7uPbIUGSDkw+KZbeM';
            break;
          case 11:
            var psr = 'U2FsdGVkX1/ZEi8DLyC6AAntqAGZvLUgqEG7iWbaxnR5eDMDsmnL2V4agw33uKBT';
            break;
          case 12:
            var psr = 'U2FsdGVkX1/UTah5ppnTpPKVgJu7MWGdbckoo/G9/zNMYiZ9WP7H+bljAqhkZkLg';
            break;
          case 13:
            var psr = 'U2FsdGVkX1+PoqNGDgFzqgeoi7bTST1CCJSGzjhA8z9aYnj0seDOn/zcC3zUGZjM';
            break;
          case 14:
            var psr = 'U2FsdGVkX1/0w73OvT3tWaYyM7sUWumasK+Cwu3y/r+LsQyRekBJlQGDNykgp6Zf';
            break;
          case 15:
            var psr = 'U2FsdGVkX1/g+mcqcmpJHTE8nLo4lpRT3a8O1usGpIzx1J7lY11TIX6zshVUEjgV';
            break;
          case 16:
            var psr = 'U2FsdGVkX1+bh6dU1+RG00nuxKsXA+d0YeNHnJydAD2Y8sEUnwemgcUgPIjYuR07';
            break;
          case 17:
            var psr = 'U2FsdGVkX1+M0MRjK2THloxPODf8xrYGSNM05KwA9oPg8KoA8KdcjHEdboeIY8bq';
            break;
          case 18:
            var psr = 'U2FsdGVkX19Pqo9A9dl4CHNn2zezsMDyizCPtLays2FWAAbz+5FiaT9ErNt5lOyA';
            break;
          case 19:
            var psr = 'U2FsdGVkX18IGExYDQ/lvp+vfPpjhqhcO9NpThf3L/YAS2xzgH2YWIBhaQQAQhJj';
            break;
          case 20:
            var psr = 'U2FsdGVkX1/f3N15M7rHII5vXJz/BZ+Unr2N/UUQIMG8p437zK5Z3PiNQXVlB3op';
            break;
          case 21:
            var psr = 'U2FsdGVkX18Q4ZOj7eYtrWCBk53jOQtDb/W/GtN/gn3M+PIWdUe9ewEsixJ/RrIbhlgIi74BsEsEfVZ3lf0HXg==';
            break;
          case 22:
            var psr = 'U2FsdGVkX18jLT/QQfqIVadQ5X/OHdE04WYhvHuLY/nj4g62YdnAnFLtOYMhrnQO';
            break;
          case 23:
            var psr = 'U2FsdGVkX182wDecSKZvhJ37Zef4zSLYwg5kaN3+dw/Qsq4VsNyWqE337/zVtz3Z';
            break;
          case 24:
            var psr = 'U2FsdGVkX1+fRZl7FHXI4agWWbS4d1VO+jGBzr+JjFC270L7976nrM1Aite4FvXO';
            break;
          case 25:
            var psr = 'U2FsdGVkX181927OkthtKszQnVADozBClPa37Unw3QU1tS/sMYarUyeP62QrgQ9c';
            break;
          case 26:
            var psr = 'U2FsdGVkX1/ETIfvns1y+tpLSEebrcYQUQuyBUBNYIbGrK8xHCgpE2oO4RD3iS5z';
            break;
          case 27:
            var psr = 'U2FsdGVkX1/fyix/wrrI0BPFMAZW497Y2+UZ28GvY5zMPDfEOprxsjh1uqmADSQj';
            break;
          case 28:
            var psr = 'U2FsdGVkX18k0hDU+9noHPhayZa+7CeCVyFfNs8lol0DFuhXtZy+wpoI52vyrylu';
            break;
          case 29:
            var psr = 'U2FsdGVkX19CO0XLtlNqcJfiOtlcAi2jMCoXyPnLlOJKczdCkp+/M+RUc3vlocMq';
            break;
          case 30:
            var psr = 'U2FsdGVkX1+bhOpna/mTt5QfXWpPsBaSZRa/xCbNp5qtxKcRnShfGnV4K8LjYz8m';
            break;
          case 31:
            var psr = 'U2FsdGVkX1+yc/CDZIkmDfyVHLpzn5s/BHQTX4s14b0=';
            break;
          case 32:
            var psr = 'U2FsdGVkX1/xhtl5h6nU2cidYAMykreHBphfoCIp2O/F8NKFIYUxuY/erER6c1DX';
            break;

          default:
          var psr = 'U2FsdGVkX19R8PDEsSWPXxELvsOhGjjFDtW6VdahoiSl1x5Q+xgvTPazqpaUkS2V';          
        } 
        
        var plain = CryptoJS.AES.decrypt(psr, 'CryptoJS.pad.Pkcs7', "{ mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 }");
        //this.tilesChecked[0].remove();
        //this.tilesChecked[1].remove();

        this.canGet = true;
        this.tilesChecked = [];

        this.tilePairs++;
        if (this.tilePairs >= this.tileCount / 2) {
            document.getElementById('pass_p').innerHTML = plain.toString(CryptoJS.enc.Utf8);
        }
    },

    resetTiles() {
        this.tilesChecked[0].style.backgroundImage = "url(images/title.png)";
        this.tilesChecked[1].style.backgroundImage = "url(images/title.png)";

        this.tilesChecked = [];
        this.canGet = true;
    },

    startGame() {
        this.dataczasstart = new Date();
        //czyścimy planszę
        document.getElementById('pass_p').innerHTML=""
        this.divBoard = document.querySelector(".game-board");
        this.divBoard.innerText = "";

        //czyścimy planszę z ruchami
        this.divScore = document.querySelector(".game-score");
        this.divScore.innerText = "";

        //czyścimy zmienne (bo gra może się zacząć ponownie)
        this.tiles = [];
        this.tilesChecked = [];
        this.moveCount = 0;
        this.canGet = true;
        this.tilePairs = 0;

        //generujemy tablicę numerów kocków (parami)
        for (let i=0; i<this.tileCount; i++) {
            this.tiles.push(Math.floor(i/2));
        }

        //i ją mieszamy
        for (let i=this.tileCount-1; i>0; i--) {
            const swap = Math.floor(Math.random()*i);
            const tmp = this.tiles[i];
            this.tiles[i] = this.tiles[swap];
            this.tiles[swap] = tmp;
        }

        for (let i=0; i<this.tileCount; i++) {
            const tile = document.createElement("div");
            tile.classList.add("game-tile");
            this.divBoard.appendChild(tile);

            tile.dataset.cardType = this.tiles[i];
            tile.dataset.index = i;
            tile.style.left = 5+(tile.offsetWidth+10)*(i%this.tileOnRow) + "px";
            tile.style.top = 5+(tile.offsetHeight+10)*(Math.floor(i/this.tileOnRow)) + "px";

            tile.addEventListener("click", this.tileClick.bind(this));
        }
    }
};

document.addEventListener("DOMContentLoaded", function() {
    const startBtn = document.querySelector(".game-start");
    startBtn.addEventListener("click", () => memoryGame.startGame());
});
