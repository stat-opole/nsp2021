const continueButton = document.querySelector('#continueButton');
const crossword = document.querySelector('#crossword');
const questions = document.querySelector('#questions');
const modal = document.querySelector('#crosswordCompletedModal');
let choosenPassword;

const passwords = [
    {
        password: 'hasła brak .....',
        questions: [
            {
                question: 'Pytanie 1: ........ ALICJA',
                hint: 'https://stat.gov.pl',
                answer: CryptoJS.AES.decrypt('U2FsdGVkX18urMWypwABOiM9wYbBzKcbbjY3sRPGYuQ=', 'CryptoJS.pad.Pkcs7', "{ mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 }").toString(CryptoJS.enc.Utf8),                
                mainLetter: CryptoJS.AES.decrypt('U2FsdGVkX18BUoQwztGBb95/+19H/lMYrPGOCANtz6U=', 'CryptoJS.pad.Pkcs7', "{ mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 }").toString(CryptoJS.enc.Utf8), 
            },
            {
                question: 'Pytanie 2: ......... ALEKSANDRA',
                hint: 'https://stat.gov.pl',
                answer: CryptoJS.AES.decrypt('U2FsdGVkX1/PK/FVymLn7sqoiVFEEGtPXjBX8uNP9sI=', 'CryptoJS.pad.Pkcs7', "{ mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 }").toString(CryptoJS.enc.Utf8),                
                mainLetter: CryptoJS.AES.decrypt('U2FsdGVkX1+hq9UYK+WwmYordT+t/fvPpBvloxU0Wu4=', 'CryptoJS.pad.Pkcs7', "{ mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 }").toString(CryptoJS.enc.Utf8), 
            },
            {
                question: 'Pytanie 3: ........ BASIA',
                hint: 'https://stat.gov.pl',
                answer: CryptoJS.AES.decrypt('U2FsdGVkX1/3TEH9l79au9m7H6e4MEpo7xidDOYENx4=', 'CryptoJS.pad.Pkcs7', "{ mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 }").toString(CryptoJS.enc.Utf8),                
                mainLetter: CryptoJS.AES.decrypt('U2FsdGVkX18BUoQwztGBb95/+19H/lMYrPGOCANtz6U=', 'CryptoJS.pad.Pkcs7', "{ mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 }").toString(CryptoJS.enc.Utf8), 
            }/*,
            {
                question: 'Pytanie 4. Kosiarka',
                hint: 'https://stat.gov.pl',
                answer: 'Kosiarka',
                mainLetter: 'o'
            },
            {
                question: 'Pytanie 5. Mieszkanie',
                hint: 'https://stat.gov.pl',
                answer: 'Mieszkanie',
                mainLetter: 'k'
            },
            {
                question: 'Pytanie 6. Worek',
                hint: 'https://stat.gov.pl',
                answer: 'Worek',
                mainLetter: 'o'
            },
            {
                question: 'Pytanie 7. Dziecko',
                hint: 'https://stat.gov.pl',
                answer: 'Dziecko',
                mainLetter: 'c'
            },
            {
                question: 'Pytanie 8. Urlop',
                hint: 'https://stat.gov.pl',
                answer: 'Urlop',
                mainLetter: 'r'
            },
            {
                question: 'Pytanie 9. Most',
                hint: 'https://stat.gov.pl',
                answer: 'Most',
                mainLetter: 'M'
            },
            {
                question: 'Pytanie 10. Most',
                hint: 'https://stat.gov.pl',
                answer: 'Most',
                mainLetter: 'M'
            },
            {
                question: 'Pytanie 11. Most',
                hint: 'https://stat.gov.pl',
                answer: 'Most',
                mainLetter: 'M'
            },
            {
                question: 'Pytanie 12. Most',
                hint: 'https://stat.gov.pl',
                answer: 'Most',
                mainLetter: 'M'
            }*/
            
        ],
        passwordCompleted: {
            description: 'tekst ..... tekst.',
            link: 'https://stat.gov.pl'
        }
    }
]


choosenPassword = 0;
prepareCrossword(choosenPassword);
prepareQuestions(choosenPassword);
prepereCompleteModal(choosenPassword);
const finishButton = document.querySelector('#buttonFinish');

finishButton.addEventListener('click', () => {
      checkFinish(choosenPassword);
  })


  function prepareCrossword(password) {
    let crosswordContent = '';
    let positionOfFirstAnswerMainLetter;
    let index = 0;
    let minMarginLeft;
    for (let answer of passwords[password].questions) {
        crosswordContent =
        `
            <div class="answer answer${index}">
            <p class="index">${index+1}.</p>
        `

        let isMainCharacterSet = false;
        let mainCharacterPosition = 0;
        let i = 0;

        for (let letter of answer.answer) {
            if (letter.toLowerCase() === answer.mainLetter.toLowerCase() && !isMainCharacterSet) {
                crosswordContent += 
                `
                    <input class="letter main-letter" type="text" maxlength="1">
                `
                isMainCharacterSet = true;
                mainCharacterPosition = i;
                if (index == 0) {
                    positionOfFirstAnswerMainLetter = i;
                }

            }
            else if (letter === ' ') {
                crosswordContent += 
                `
                    <input class="space letter" type="text" maxlength="0" disabled>
                `
            }
            else {
                crosswordContent += 
                `
                    <input class="letter" type="text" maxlength="1">
                `
            }
            i++;
        }
        crosswordContent += 
        `
         </div> 
        ` 

        crossword.innerHTML += crosswordContent;
        
        let marginLeft = calculatePosition(positionOfFirstAnswerMainLetter, mainCharacterPosition);
        if (index == 0) {
            minMarginLeft = marginLeft;
        }
        else {
            if (minMarginLeft > marginLeft) minMarginLeft = marginLeft;
        }

        let div = crossword.querySelector(`.answer${index}`);
        div.style.marginLeft = marginLeft+'px';
        index++;
    }

    const windowWidth = window.innerWidth;
    if (windowWidth < 500) {
        crossword.style.left = `${Math.abs(minMarginLeft) + 40}px`;
    }

    let answersIndex = crossword.querySelectorAll('.index');
    answersIndex.forEach(index => {
        if (index.innerHTML.slice(0, -1) < 10) {
            index.style.left="-25px";
        }
        else {
            index.style.left="-40px";
        }
    })

    const letters = crossword.querySelectorAll('.letter');
    letters.forEach(letter => {
        letter.addEventListener('input', (e) => {
            if (e.target.value.length > 1) {
                e.target.value.slice(0, 1);
            }
            else if (e.target.value.length === 1) {
                let next = e.target;
                while (next = next.nextElementSibling) {
                    if (next == null) {
                        break;
                    }
                    if (next.tagName.toLowerCase() === 'input') {
                        if (next.disabled === true) {
                            continue;
                        }
                        next.focus();
                        break;
                    }
                }
            }
            else if (e.target.value.length === 0) {
                let previous = e.target;
                while (previous = previous.previousElementSibling) {
                    if (previous == null) {
                        break;
                    }
                    if (previous.tagName.toLowerCase() === 'input') {
                        if (previous.disabled === true) {
                            continue;
                        }
                        previous.focus();
                        previous.select();
                        break;
                    }
                }
            }
        })
    })
}

function prepareQuestions(password) {
    let questionsContent = '';
    questionsContent = `
        <ol>
    `

    for (let question of passwords[password].questions) {
        questionsContent += `
            <li>${question.question} <a class="hint" target="_blank" href="${question.hint}">[?]</a></li>
        `
    }

    questionsContent += `
        </ol>
        <button id="buttonFinish" class="btn button--finish">
            <svg>
                <rect x="0" y="0" fill="none" width="100%" height="100%"/>
            </svg>
            Sprawdź
        </button>
    `
    questions.innerHTML = questionsContent;
}

function prepereCompleteModal(password) {
    const crosswordChoosenPassword = passwords[password];
    let modalContent = '';
    /*modalContent = `
        <div class="modal-header">
            <p class="modal-title">Gratulacje! Już znasz hasło!</p>
            <p class="modal-title-password">${crosswordChoosenPassword.password} </p>
        </div>
        <div class="modal-content">
            <p class="description">${crosswordChoosenPassword.passwordCompleted.description}</p>
            <p>Wiecej informacji znajdziesz <a href="${crosswordChoosenPassword.passwordCompleted.link}" target="_blank">tutaj</a></p>
        </div>
        <div class="modal-footer">
            <button id="buttonCloseModal" class="btn button--close close">
                <svg>
                    <rect x="0" y="0" fill="none" width="100%" height="100%"/>
                </svg>
                Zamknij
            </button>
        </div>
    `*/

    const modal = document.querySelector('#crosswordCompletedModal');
    modal.innerHTML = modalContent;

    const closeButtons = modal.querySelectorAll('.close');
    closeButtons.forEach(button => {
        button.addEventListener('click', () => {
            modal.style.display="none";
        })
    })
}

function calculatePosition(positionA, positionB) {  
    let windowWidth = window.innerWidth;
    let inputWidth;
    if (windowWidth <= 550) {
        inputWidth = 17;
    }
    else if (windowWidth <= 880) {
        inputWidth = 22;
    }
    else {
        inputWidth = 36;
    }
    
    let diffrence = positionA - positionB;
    let result = diffrence * inputWidth;
    return result;
}

function checkFinish(password) {
    const answers = crossword.querySelectorAll('.answer');
    const amountOfQuestions = passwords[password].questions.length;
    let amountOfGoodAnswers = 0;
    answers.forEach((answer,index) => {
        const letters = answer.querySelectorAll('.letter');
        const questionAnswer = passwords[password].questions[index].answer;
        let userAnswer = '';
        letters.forEach(letter => {
            userAnswer += letter.value;
            if (letter.classList.contains('space')) userAnswer+= ' '; 
        })

        if (userAnswer.toLowerCase() == questionAnswer.toLowerCase()) {
            letters.forEach(letter => {
                if (!letter.classList.contains('space')) {
                    letter.classList.add('good-letter');
                    letter.classList.remove('bad-letter');
                }
            })
            amountOfGoodAnswers++;
        }
        else {
            letters.forEach(letter => {
                if (!letter.classList.contains('space')) {
                    letter.classList.add('bad-letter')
                    letter.classList.remove('good-letter');
                }
            })
        }
    })

    console.log(amountOfGoodAnswers);

    if (amountOfGoodAnswers == amountOfQuestions) {
        modal.style.display = 'block';
    }
}
