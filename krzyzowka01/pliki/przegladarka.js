//detects if user uses Internet Explorer //returns version of IE or false, if browser is not IE  //Function to detect IE or not 
function IEdetection() { 
    var ua = window.navigator.userAgent; 
	var msie = ua.indexOf('MSIE '); 
	if (msie > 0) { // IE 10 or older, return version number 
	  return ('Niepoprawna przeglądarka - proszę użyć Chrome, FireFox lub Edge'); 
	} 
	var trident = ua.indexOf('Trident/'); 
	if (trident > 0) { // IE 11, return version number 
	  var rv = ua.indexOf('rv:'); 
	  return ('Niepoprawna przeglądarka - proszę użyć Chrome, FireFox lub Edge');  
	} 
	var edge = ua.indexOf('Edge/'); 
	if (edge > 0) { //Edge (IE 12+), return version number 
	  return (''); 
	} 
	// User uses other browser 
	return (''); 
	} 
	var result = IEdetection(); 
	document.getElementById('IE').innerHTML = result
